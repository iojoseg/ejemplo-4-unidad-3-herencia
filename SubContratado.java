public class SubContratado{

public float cantHoras;
public float precioHora;

public SubContratado(){
cantHoras = 0.0f;
precioHora = 0.0f;
}

public SubContratado(float cantHoras, float precioHora){
this.cantHoras = cantHoras;
this.precioHora = precioHora;
}
public String toString(){
return "[Cantidad de Horas = "+ cantHoras + ", Precio Hora = "+ precioHora+ "]";
}

public float calcularPago(){
return cantHoras * precioHora;
}



}