public class Main{

public static void main(String args[]){

   Empleado empleado1 = new Empleado();
   
   System.out.println("Nombre = " + empleado1.getNombre());
   System.out.println("Edad = " + empleado1.getEdad());
   
   System.out.println("-----------------");
   
   System.out.println(empleado1.toString());
   
   System.out.println("-----------------");
   System.out.println("-----------------");
   
   Empleado empleado2 = new Empleado("Sofia Lara", 40);
   System.out.println(empleado2.toString());
   
   System.out.println("-----------------");
   System.out.println("-----------------");
   
   empleado2.setNombre("Carmen Guzman");
   empleado2.setEdad(41);
   
   System.out.println(empleado2.toString());
   
    System.out.println("-----------------");
    System.out.println("Clase Asalariado");
    System.out.println("-----------------");
   
    Asalariado asalariado1 = new Asalariado("Daniel Perez", 35, 7500.0f);
    
    SubContratado subCon = new SubContratado(12.0f,200.0f);
    System.out.println(subCon.toString());
    System.out.println("Pago SubContratado = "+subCon.calcularPago());
   

  
  

 }
}