public class Asalariado extends Empleado{

private float sueldo;

public Asalariado(){
sueldo = 0.0f;
}

public Asalariado(String nombre, int edad, float sueldo){
super(nombre,edad);
this.sueldo = sueldo;
}

public String toString(){
return "[Sueldo = "+ sueldo + "]";
}

// Falta implementar el metodo
public float calcularPago(){
return sueldo;
}


}